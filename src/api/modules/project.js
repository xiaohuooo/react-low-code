import serve from "../index";

/**
 * @name 项目模块
 */
// * 用户项目列表
export const projectList = (params) => {
	return serve.post(`/temp/list`, params);
};

// bandwidth:<integer>
// country:<string>
// details:<object>
// region:<string>
// site:<string></string>
export const createTopology = (params) => {
	return serve.post(`/enterprise2_0/api/v1/topology/create_topology/`, params);
};
