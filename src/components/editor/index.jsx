import React, { useState } from 'react';
import Left from './Left';
import Right from './Right';
import Canvas from './Canvas';
import { HTML5Backend } from 'react-dnd-html5-backend';
import { DndProvider } from 'react-dnd';
function Editor() {
  return (
    <DndProvider backend={HTML5Backend}>
      <div className="h-screen flex flex-col text-gray-600">
        <main className="flex-1 overflow-hidden flex">
          <Left />
          <Canvas />
          <Right />
        </main>
      </div>
    </DndProvider>
  );
}
export default Editor;
