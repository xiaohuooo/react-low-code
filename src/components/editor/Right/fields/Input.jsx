import React, { ReactElement } from 'react'
import { Input } from 'antd'


export default function CustomInput({ onChange, value }) {
  return (
    <input
      type="text"
      value={value}
      onChange={(e) => onChange(e.target.value)}
    />
  )
}

export function TextArea({ onChange, value }) {
  return (
    <Input.TextArea
      rows={4}
      value={value}
      onChange={(e) => onChange(e.target.value)}
    />
  )
}
