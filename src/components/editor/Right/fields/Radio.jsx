import React, { ReactElement } from 'react'
import { Radio } from 'antd'


export default function RadioField({
  onChange,
  value,
  ...other
}) {
  return (
    <Radio.Group
      optionType="button"
      onChange={(e) => onChange(e.target.value)}
      value={value}
      {...other}
    ></Radio.Group>
  )
}
