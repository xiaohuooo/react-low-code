export function isParentNode(type) {
  return ['div','building','floor', 'Grid', 'Form', 'Form.Item'].indexOf(type) > -1
}
