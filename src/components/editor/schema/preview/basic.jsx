import React, { ReactElement } from 'react';
import cl from 'classnames';

function Grid({
  cols,
  rows,
  gapX,
  gapY,
  ...other
}) {
  return (
    <div
      {...other}
      className={cl('grid', {
        [`grid-cols-${cols}`]: cols,
        [`grid-rows-${rows}`]: rows,
        [`gap-x-${gapX}`]: gapX,
        [`gap-y-${gapY}`]: gapY,
      })}
    />
  );
}


const previewFields = {
  div: (props) => <div {...props} />,
  building: (props) => <div {...props} >building {props.children}</div>,
  floor: (props) => <div {...props} >floor {props.children}</div>,
  room: (props) => <div {...props} >room {props.children}</div>,
  // eslint-disable-next-line jsx-a11y/heading-has-content
  h1: (props) => <h1 {...props} />,
  p: (props) => <p {...props} />,
  span: (props) => <span>{props.children}</span>,
  Link: (props) => <a {...props} />,
  img: (props) => <img {...props} alt="" />,
  input: (props) => <input {...props} />,
  select: ({ children, ...other }) => (
    <select {...other}>
      {children.map((option) =>
        option.value && option.label ? (
          <option value={option.value} key={option.value}>
            {option.label}
          </option>
        ) : null
      )}
    </select>
  ),
  button: (props) => <button {...props} />,
};

export default previewFields;
