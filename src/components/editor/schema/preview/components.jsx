import React from 'react';
import cl from 'classnames';

function Grid({
  cols,
  rows,
  gapX,
  gapY,
  ...other
}) {
  return (
    <div
      {...other}
      className={cl('grid', {
        [`grid-cols-${cols}`]: cols,
        [`grid-rows-${rows}`]: rows,
        [`gap-x-${gapX}`]: gapX,
        [`gap-y-${gapY}`]: gapY,
      })}
    />
  );
}

const previewFields = {
  Grid,
};

export default previewFields;
