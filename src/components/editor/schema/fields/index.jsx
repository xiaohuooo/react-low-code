import antd from './antd'
import basic from './basic'
import DragPanel from '../../Left/DragPanel'
import TreePanel from '../../TreePanel'
import React, { ReactElement } from 'react'

const menus = [
  {
    key: 'tree',
    icon: 'tree',
    panel: <TreePanel />,
  },
  {
    key: 'basic',
    icon: 'HTML5',
    panel: <DragPanel data={basic} />,
  },
  {
    key: 'antd',
    icon: 'antd',
    panel: <DragPanel data={antd} />,
  }
]

export default menus
