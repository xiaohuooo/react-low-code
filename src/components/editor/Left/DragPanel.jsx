import React, { ReactElement } from 'react'
import DragItem from './DragItem'



export default function DragPanel({ data }) {
  return (
    <>
      {data.map((d) => (
        <DragItem key={d.type} data={d} />
      ))}
    </>
  )
}
