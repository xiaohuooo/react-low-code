import { configureStore, ThunkAction, Action } from '@reduxjs/toolkit';
import counterReducer from './features/counter/counterSlice';
import tokenReducer from './features/tokenSlice';
import menuReducer from './features/menuSlice';
import globalReducer from './features/globalSlice';
import codeTree from './features/codeTreeSlice';
export const store = configureStore({
  reducer: {
    counter: counterReducer,
    token:tokenReducer,
    menu:menuReducer,
    global:globalReducer,
    codeTree,
  },
  // middleware: [],
});