import { createSlice, PayloadAction } from '@reduxjs/toolkit';


const initialState = {
  value: []
};

export const menuSlice = createSlice({
  name: 'menu',
  initialState,
  reducers: {
    setMenu: (state, action) => {
      state.value = action.payload;
    }
  }
});

export const { setMenu } = menuSlice.actions;
export const selectMenu = (state) => state.menu.value;

export default menuSlice.reducer;
