import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { RootState } from '@/store';


const initialState = {
  value: localStorage.getItem('token')||''
};

export const tokenSlice = createSlice({
  name: 'token',
  initialState,
  reducers: {
    setToken: (state, action) => {
      state.value = action.payload;
      localStorage.setItem('token',action.payload)
    }
  }
});

export const { setToken } = tokenSlice.actions;
export const selectToken = (state) => state.token.value;

export default tokenSlice.reducer;
