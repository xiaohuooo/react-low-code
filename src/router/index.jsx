import { Navigate, useRoutes } from 'react-router-dom';
import Layout from '@/components/Layout/index';
import Home from '@/views/home/index';

export const rootRouter = [
  {
    path: '/',
    element: <Navigate to="/home" />,
  },
  {
    element: <Layout />,
    children: [
      {
        path: 'home',
        element: <Home />,
      }
    ],
  },
  {
    path: '*',
    element: <Navigate to="/404" />,
  },
];

const Router = () => {
  const routes = useRoutes(rootRouter);
  return routes;
};

export default Router;
