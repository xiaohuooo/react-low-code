/**
 * depth first traverse, from root to leaves, children in inverse order
 *  if the fn returns false, terminate the traverse
 */
export const traverse = (
    data,
    fn
  ) => {
    if (fn(data) === false) {
      return false
    }
  
    if (data && data.children) {
      for (let i = data.children.length - 1; i >= 0; i--) {
        if (!traverse(data.children[i], fn)) return false
      }
    }
    return true
  }
  
  /**
   * depth first traverse, from leaves to root, children in inverse order
   *  if the fn returns false, terminate the traverse
   */
  export const traverseUp = (
    data,
    fn
  ) => {
    if (data && data.children) {
      for (let i = data.children.length - 1; i >= 0; i--) {
        if (!traverseUp(data.children[i], fn)) return
      }
    }
  
    if (fn(data) === false) {
      return false
    }
    return true
  }
  